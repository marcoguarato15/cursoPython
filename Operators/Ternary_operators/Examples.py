raining = False
print('Today is ' +('sunny.','raining.')[raining])

# or

print('Today is ' + ('raining.' if (raining) else 'sunny.'))