# Make a solution to discover whats the percentage of deficit based on your salary
#Ex:
# salary 1000
# deficit 100
# answer = 10%

salary = 1750
deficit = 876
answer = (deficit / salary) * 100

print(f'You salary deficit per month in percentage is: {answer} %.')
# other ways to answer withou getting concatenation problems
print('Deficit of salary in percentage: ' + str(answer) + ' %.')
print('Deficit in percentage: {} %'.format(answer))
print('Deficit in %: ', answer)

