# is , is
print("IS\n")
a = 3
b = a
c = 3

print("only works in integers and this kind of variables, because it's comparing the memory slot it's taking"
 + "\nb -> a -> 3 <- c")
print('case 1:\na is b : ', a is b)
#same scenario as above
print('a is c: ',a is c)

list_a = [1, 2, 3]
list_b = list_a
list_c = [1, 2, 3]

print('in this case the second example it goes false because the pointer in list_a is in a diferent place than the pointer in list_c \n' +
      ' list_a ->  [1, 2, 3] <- list_b        list_c -> [1, 2, 3] ')

print('\n\ncase 2:\nlist_a is list_b : {list_a is list_b}', list_a is list_b)
print('list_a is list_c {list_a is list_c}', list_a is list_c)
print()
# in
print("IS IN\n works for verifying if there is the value inside a list or dictionary")

print('a = 3-> a in list_a ([1, 2, 3])', a in list_a)
c = 4
print('c = 4-> c in list_a: ', c in list_c)

#this example does not work becaus 'list' in unhashable
dictionary = {'a': [1, 2, 3]}
#print("dictionary = {'a': [1, 2, 3]} in list_a", list_a in dictionary)
a = 3 + 5