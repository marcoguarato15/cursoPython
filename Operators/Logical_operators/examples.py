# >> and << 

print('Example: To go to shopping is necessary a: Sunny day >> and << money on account')
print("It's sunny and you have money (True and True): ", True and True)
print("It's sunny but you no money (True and False)", True and False)
print("It's raining but you have money (False and True): ", False and True)
print(f"It's raining and you haveno money (False and False): {False and False} \n\n")

# >> or << 

print('Example: To go to shopping is necessary a: Sunny day >> or << money on account')
print("It's sunny and you have money (True and True): ", True or True)
print("It's sunny but you no money (True and False): ", True or False)
print("It's raining but you have money (False and True): ", False or True)
print(f"It's raining and you haveno money (False and False): {False or False}")

# >> XOR << (exclusively one or another)

print('Example: Chosing between chocolate or biscuits')
print('Chocolate and biscuits (True != True)', True != True)
print('Only chocolate (True != False)', True != False)
print('Only Biscuit (False != True)', False != True)
print(f'None of them (False != False) {False != False} \n')

# >> not << Negates the operator

print(f'Examples: Not saying the truth (not True): {not True} and not saying a lye (not False): {not False}')
