def multiplicar(x):
    def calcular(y):
        return x * y
    return calcular

if __name__ == "__main__":  
    # exemplo de closure e Higher order function, está armazenando o valor da funcal calcular
    # exemplo de lazy evaluation, esta esperando outro parametro para finalizar a funcao
    dobrar = multiplicar(2)
    triplo = multiplicar(3)
    print(dobrar)
    print(triplo)

    # exemplo de lazy evaluation, estava esperando o outro valor que
    # era umfa funcao  'calcular(y)'
    print(f'dobro de 7: {dobrar(7)}')
    print(f'dobro de 3: {triplo(3)}')