#!/usr/bin/python3

# map serve para mapear elementos de uma lista para outra lista/ elemento
    
# a função map itera os dois parametros o primeiro que é a função a ser realizada
# com o segundo que é o objeto Iterable
# retorna um objeto map que pode ser transformado em uma lista/tupla
lista_1 = [1, 2 ,3]
dobro = map(lambda x: x * 2, lista_1)

print(list(dobro))

lista_2 = [
    {'nome': 'Joao', 'idade': 25},
    {'nome': 'Maria', 'idade': 30},
    {'nome': 'José', 'idade': 45}
]

so_nome = map(lambda nome: nome['nome'], lista_2)
print(list(so_nome))

so_idade = map(lambda idade: idade['idade'], lista_2)

so_idade = list(so_idade)
print(so_idade)
print(sum(so_idade))


str_nome_idade = map(lambda nome,idade: f'Nome: {nome["nome"]}, idade: {idade["idade"]} ', lista_2,lista_2)
# ou mais correto \/  \/  \/
# str_nome_idade = map(lambda pessoa: f'Nome: {pessoa["nome"]}, idade: {pessoa["idade"]} ', lista_2)
print(list(str_nome_idade))