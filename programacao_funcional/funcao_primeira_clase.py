#!/usr/bin/python3

# exemplos de programação funcional
# exemplo de << First Class Function >>
# função é armazenada em uma lista

def dobro(x):
    return x * 2

def quadrado(x):
    return x ** 2

if __name__ == '__main__':
    # realiza dobro e quadrado alternadamente 10 vezes
    listaFunc = [dobro, quadrado] * 5

    # zip junta a lista de funções e o range de funções e ao percorrer o zip
    # é realizada essa iteração do laço for
    for func, numero in zip(listaFunc, range(1,11)):
        print(f'O {func.__name__} de {numero} é: {func(numero)}')