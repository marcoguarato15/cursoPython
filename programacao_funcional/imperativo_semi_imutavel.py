#!/usr/bin/python3
from locale import setlocale, LC_ALL
from calendar import mdays, month_name

# Portugues do Brasil
setlocale(LC_ALL, 'pt_BR.utf8')

# Listar todos os meses do ano com 31 dias

# a imutabilidade esta em não alterar os dados enviados para as funcões
# e sim enviar um dado e retornar outro a partir desse dado sem alterações no primeiro

if __name__ == "__main__":
    print('Meses com 31 dias: ')
    for mes in range(1,13):
        if mdays[mes] == 31:
            print(f'- {month_name[mes]}')
   