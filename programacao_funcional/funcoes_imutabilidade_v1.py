#!/usr/bin/python3
from functools import reduce
from operator import __add__ as add

valores = (30, 10, 25, 70, 100, 94)

# para dados imutáveis prefira estruturas imutáveis.

print( sorted(valores) )
print( min(valores) )
print( max(valores) )
print( sum(valores) )
print( reduce(add, valores))
print( tuple( reversed(valores) ) )
print( valores )

# não é possivel ser feito na tupla, porem foi exemplo em uma lista
# para mostrar que os dados são alterados nesses casos.
# valores.sort()
# valores.reverse()
# print( valores )

print(valores)