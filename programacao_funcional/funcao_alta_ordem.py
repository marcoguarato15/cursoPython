#!/usr/bin/python3

# exemplos de programação funcional
# exemplo de << Higher Order Funtion >>
# função é armazenada enviada/retornada para/por outra função

from funcao_alta_ordem import dobro, quadrado

def processar(titulo, lista, funcao):
    print(f'Processando: {titulo}')
    for i in lista:
        print(i, '=>', funcao(i))

if __name__ == "__main__":
    processar('dobros de 1 a 10', range(1,11), dobro)
    processar('quadrados de 1 a 10', range(1,11), quadrado)