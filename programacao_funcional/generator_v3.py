#!/usr/bin/python3

def sequence():
    # yield traduzido é produção
    num = 0
    while True:
        num += 1
        yield num


if __name__ == '__main__':

    sequencia = sequence()
    
    print(next(sequencia))
    print(next(sequencia))
    print(next(sequencia))
    print(next(sequencia))
    print(next(sequencia))
    print(next(sequencia))
    print(next(sequencia))
    print(next(sequencia))
    print(next(sequencia))