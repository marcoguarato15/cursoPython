#!/usr/bin/python3
from locale import setlocale, LC_ALL
from calendar import mdays, month_name
from functools import reduce

# Portugues do Brasil
setlocale(LC_ALL, 'pt_BR.utf8')

# Listar todos os meses do ano com 31 dias

if __name__ == "__main__":

    # print(list(month_name))
    dias = map(lambda days,nomeMes: {days: nomeMes}, mdays, month_name)
    # print(list(dias))
    meses = filter(lambda dia: list(dia.keys())[0] == 31 , dias)

    for dados in meses:
        mes = list(dados.values())[0]
        dia = list(dados.keys())[0]
        print(f' {mes} tem {dia} dias')
