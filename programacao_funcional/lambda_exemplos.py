#!/usr/bin/python3

#exemplo de dobrar os itens da lista 'a1'
# a1 = [1, 2, 3]
# a2 = []
# for i in a1:
#     a2.append(i*2)
# print(a2)

a = [1, 2, 3]
# lambda é uma função anonima
# i são os elementos da lista 'a'
# lambda possui um retorno implícito

# lambda de dobrar o valor
m = map(lambda i: i * 2, a)
print(m)
print(list(m))


# lambda de elevar ao quadrado
m = map(lambda i: i ** 2, a)
# map serve para mapear elementos de uma lista para outra lista/ elemento
print(tuple(m))