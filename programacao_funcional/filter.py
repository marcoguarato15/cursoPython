#!/usr/bin/python

pessoas = [
    {'nome': 'Pedro', 'idade': 11},
    {'nome': 'Mariana', 'idade': 18},
    {'nome': 'Rebeca', 'idade': 26},
    {'nome': 'Artur', 'idade': 23},
    {'nome': 'Paulo', 'idade': 19},
    {'nome': 'Tiago', 'idade': 17},
    {'nome': 'Gabriela', 'idade': 13},
    {'nome': 'Augusto', 'idade': 21}
]
# Como a função 'map' possui 2 parametros, uma função e um objeto 'iterable'

# define critérios sem utilizar 'for's e 'if's
# utiliza-se bastante lambda para essa função
# DEVE retornar verdadeiro ou falso
menores = list(filter(lambda p: p['idade'] < 18, pessoas))
print(menores)

nome_maior_que_6 = list(filter(lambda p: len(p['nome']) > 6, pessoas))
print(nome_maior_que_6)