#!/usr/bin/python3
# implementação simplificada do map


def mapear(funcao, lista):
    return (funcao(x) for x in lista)


if __name__ == '__main__':
    resultado = mapear(lambda x: x ** 2, [2, 3, 4])
    print(next(resultado))
    print(next(resultado))
    print(next(resultado))
    # print(next(resultado))
    print(list(mapear(lambda x: x ** 2, [2, 3, 4])))