#!/usr/bin/python3

compras = (
    {'quantidade': 2, 'preco': 10},
    {'quantidade': 3, 'preco': 20},
    {'quantidade': 5, 'preco': 14},
    )

def calc_preco_total(compra):
    return compra['quantidade'] * compra['preco']

totais = tuple (
    # map serve para mapear elementos de uma lista para outra lista/ elemento
    
    # a função map itera os dois parametros o primeiro que é a função a ser realizada
    # com o segundo que é o objeto Iterable
            map(calc_preco_total,compras)
        )

print('Preços totais: ', totais)
print('Totais geral: ', sum(totais))