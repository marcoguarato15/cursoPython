#!/usr/bin/python3

from functools import reduce

# função muito flexivel, possivel a partir de uma lista 
# gerar outra lista/dicionario/ unico numero

pessoas = [
    {'nome': 'Pedro', 'idade': 11},
    {'nome': 'Mariana', 'idade': 18},
    {'nome': 'Rebeca', 'idade': 26},
    {'nome': 'Artur', 'idade': 23},
    {'nome': 'Paulo', 'idade': 19},
    {'nome': 'Tiago', 'idade': 17},
    {'nome': 'Gabriela', 'idade': 13},
    {'nome': 'Augusto', 'idade': 21}
]

# a cada iteração ele ira somando idades a anterior


somenteIdades = list(map(lambda idades: idades['idade'], pessoas))
menorIdade = list(filter(lambda idade: idade < 18, somenteIdades))
somaDeMenorIdade = reduce(lambda idades, idade: idade + idades, menorIdade)

print('Somente as Idades:', somenteIdades)
print('Menores de idade:', menorIdade)
print('Soma dos menores de idade:', somaDeMenorIdade)
print()
filtroMenorIdades = list(filter(lambda MenoresIdade: MenoresIdade['idade'] < 18, pessoas))
somaDeMenorIdades = reduce(lambda idades, idade: idades + idade['idade'],filtroMenorIdades, 0)
print('Filter e Reduce:(Soma todos Menores de idade)', somaDeMenorIdade)

print()
somaTodasIdades = reduce(lambda idades, p: idades + p['idade'], pessoas, 0)
print('Função Reduce (somaTodasIdades):', somaTodasIdades)