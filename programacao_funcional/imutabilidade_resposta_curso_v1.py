#!/usr/bin/python3
from locale import setlocale, LC_ALL
from calendar import mdays, month_name
from functools import reduce

# Portugues do Brasil
setlocale(LC_ALL, 'pt_BR.utf8')

# Listar todos os meses do ano com 31 dias

if __name__ == "__main__":
    # 1. (filter) pegar os indices de todos os meses com 31 dias
    # 2. (map) transformar os índices em nome
    # 3. (reduce) Juntar tudo para imprimir

    numeroMesCom31Dias = filter(lambda x: mdays[x] == 31 , range(1,13))

    nomeMes31Dias = map(lambda indice: month_name[indice], numeroMesCom31Dias)

    stringResultado = reduce(lambda todos,nomeMes: f'{todos}\n- {nomeMes}',nomeMes31Dias, 'Meses com 31 dias:')
    print(stringResultado)
