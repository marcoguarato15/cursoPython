#!/usr/bin/python3
from locale import setlocale, LC_ALL
from calendar import mdays, month_name
from functools import reduce

# Portugues do Brasil
setlocale(LC_ALL, 'pt_BR.utf8')

# Listar todos os meses do ano com 31 dias

# a imutabilidade esta em não alterar os dados enviados para as funcões
# e sim enviar um dado e retornar outro a partir desse dado sem alterações no primeiro

def meses_com_31(mes):return mdays[mes] == 31

def get_nome_mes(mes): return month_name[mes]

def juntar_meses(todos, nome_mes): return f'{todos}\n- {nome_mes}'

if __name__ == "__main__":
    # 1. (filter) pegar os indices de todos os meses com 31 dias
    # 2. (map) transformar os índices em nome
    # 3. (reduce) Juntar tudo para imprimir

   print(reduce(juntar_meses ,
                map(get_nome_mes, filter(meses_com_31, range(1,13))), 
                'Meses com 31 dias:'))
   
