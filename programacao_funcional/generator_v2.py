#!/usr/bin/python3

def cores_arco_iris():
    # yield traduzido é produção
    yield 'vermelho'
    yield 'laranja'
    yield 'amarelo'
    yield 'verde'
    yield 'azul'
    yield 'índigo'
    yield 'violeta'


if __name__ == '__main__':
    generator = cores_arco_iris()

    print(type(generator))
    # for trata internamente o erro StopIteration
    for cor in generator:
        print(cor)
    print('Fim!')