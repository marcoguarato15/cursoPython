#!/usr/bin/python3

# programação funcionar
# Closure

#funcao de alta ordem(retorna uma função), avaliação tardia(armazena 'x' na primeira instancia) e CLOSURE(retornando a função calcular)
def multiplicar(x):
    def calcular(y):
        return x * y
    return calcular

if __name__ == '__main__':
    # retornou calcular com x de multiplicar = 2
    # (closure tem a noção desse 'x')
    
    dobro = multiplicar(2)
    print(dobro)
    print('Função multiplicar."calcular" < função que está armazenada \n')
    triplo = multiplicar(3)
    print(triplo)
    print(f'triplo de 3: {triplo(3)}')
    print(f'dobro de 4: {dobro(4)}')
