#with sempre garante o fechamento do arquivo.

with open('pessoas.csv') as arquivo:
    with open('pessoas.txt', 'w') as saida:
        for registros in arquivo:
            pessoas = registros.strip().split(',')
            print('Nome: {}, idade: {}'.format(*pessoas), file=saida)

if arquivo.closed:
    print('Arquivo foi fechado.')