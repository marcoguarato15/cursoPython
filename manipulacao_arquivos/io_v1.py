# O '*' significa que irá acessar todos os dados internos da
# estrutura de dados. como no splitlines() retorna uma lista dos dados
# com a quebra de linha sendo seu split

arquivo = open('pessoas.csv')
dados = arquivo.read()
arquivo.close()

for dado in dados.splitlines():
    print('Nome: {}, idade: {}'.format(*dado.split(',')))
