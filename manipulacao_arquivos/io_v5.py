#with sempre garante o fechamento do arquivo.

with open('pessoas.csv') as arquivo:
    for registros in arquivo:
        print('Nome: {}, idade: {}'.format(*registros.strip().split(',')))

if arquivo.closed:
    print('Arquivo foi fechado.')