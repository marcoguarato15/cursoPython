from pacote1.modulo1 import soma
from pacote2.modulo1 import subtracao as sub

#ao colocar a função com vírgula, ele concatena um espaço em branco automaticamente.
print('Soma:', soma(3, 2))
print('Subtracao:', sub(4,2))