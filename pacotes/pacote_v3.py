from pacote1 import modulo1
from pacote2 import modulo1 as modulo1_sub

print (f'Soma do modulo 1 pacote 1: {modulo1.soma(3, 4)}')
print (f'Subtração do modulo 1 pacote 2: {modulo1_sub.subtracao(4, 3)}')