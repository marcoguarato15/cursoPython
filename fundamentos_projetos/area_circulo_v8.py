from math import pi


def circulo(raio):
    print('Circle area: {}'.format( pi * float(raio) ** 2))


if __name__ == '__main__':
    raio = input('Inform the radius of the circle: ')
    circulo(raio)
