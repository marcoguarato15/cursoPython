from math import pi

if __name__ == '__main__':
    radius = input('Inform the radius of the circle: ')
    print('Circle area: {}'.format( pi * float(radius) ** 2))

