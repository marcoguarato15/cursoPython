# -*- coding: utf-8 -*-
from math import pi
import sys
import errno


class TerminalColor:
    NORMAL = '\033[91m'
    ERROR = '\033[0m'


def circulo(raio):
    return pi * float(raio) ** 2


def help():
    print('É necessário informar o raio do circulo'
          + '\nSintaxe: {sys.argv[0]} <raio>')


if __name__ == '__main__':
    if len(sys.argv) < 2:
        help()
        sys.exit(errno.EPERM)

    if not sys.argv[1].isnumeric():
        help()
        print(TerminalColor.ERROR + 'Radius must be numeric' 
              + TerminalColor.NORMAL)
        sys.exit(errno.EINVAL)

    raio = sys.argv[1]
    area = circulo(raio)
    print(area)
