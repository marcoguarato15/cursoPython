from math import pi


def circulo(raio):
    return pi * float(raio) ** 2

# input pega o que foi escrito no terminal
if __name__ == '__main__':
    raio = input('Inform the radius of the circle: ')
    area = circulo(raio)
    print(area)
