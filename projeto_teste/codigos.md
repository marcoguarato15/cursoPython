## instalação pelo pip
```$ pip install --user Django```

## comandos pip 'venv'
- mostra os pacotes  instalados e suas versões
```$ pip3 freeze```

- instalar dependencias de um projeto através do pip (mesmo que npm)
```$ pip3 install -r requirements.txt```

## iniciar o 'venv'
- Cria o ambiente virtual sem iniciá-lo
```$ python -m venv .venv```

- Inicia o ambiente virtual (sistemas unix)
```$ source .venv/bin/activate```

- Fecha a conexão com o venv
```$ deactivate```

## Modo de uso 

- Ordem: inicia o ambiente --> baixa as dependencias/pacotes --> realiza o '$ pip freeze > requirements.txt' --> fecha a conexão com o venv

- Ordem inversa: inicia o ambiente com '$ source .venv/bin/activate' --> realiza o comando '$ pip install -r requirements.txt' para baixar os pacotes no ambiente virtual

###### OBS: o .venv estará incluído no .gitignore