# Número desconhecido de repetições

# while True:
#    print('Loop infinito...')
from random import randint


numero_digitado = -1
numero_advinhado = randint(0, 9)

while numero_digitado != numero_advinhado:
    numero_digitado = int(input('Informe o número:'))

print('Número foi advinhado, era: {}'.format(numero_advinhado))
