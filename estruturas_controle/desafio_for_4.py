from random import randint

def sortea_dado():
    return randint(1, 6)

if __name__ == '__main__':
    num_sorteado = sortea_dado()
    for i in range(1,7):
        if i % 2 != 0:
            continue

        if i == num_sorteado:
            print(f'ACERTOU: {i}')
            break
    else:
        print('NAO ACERTOU')