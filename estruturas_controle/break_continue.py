#continue = pula a iteração de determinada estrutura de controle

#se resto da operação dividido por 2 for diferente de 0 -> numeros impares <- 
for num in range (1,11):
    if num % 2 != 0:
        continue
    print(num)

#break sai do bloco da estrutura de controle
for num in range(1, 11):
    if num == 5:
        break
    print(num)

print('Fim')