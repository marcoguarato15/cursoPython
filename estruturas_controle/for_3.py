#percorrendo valores dentro de um dicionário
dict = {'Nome':'Rafael', 'idade': 16, 'Tipo Sanguineo': 'A+', 'Cor da caneta': 'Azul'}

for chave in dict:
    print(chave)

for valor in dict.values():
    print(valor)

for chave, valor in dict.items():
    print(f'{chave} = {valor}')