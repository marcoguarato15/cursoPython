for caracteres in 'testando print alterando end':
    print(caracteres, end=',')
# irá continuar após o último caracter, como entre este print e o próximo não teve o \n para quebrar a linha
for numero in [1, 2, 3, 4, 5, 6]:
    print('Numero: {}'.format(numero))

for aprovados in ['Rafaela', 'Joao', 'Maria', 'Gabriel']:
    print('Nome dos aprovados: ', aprovados)

for dias_semana in ('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'):
    print(f'Hoje é dia: {dias_semana}')

for posicao, colocados in enumerate(['Rafaleia', 'Joao', 'Pedro']):
    print(f'{posicao + 1}) {colocados}')

#   set(conjunto) não foi criado a partir do metodo literal
#   {'joselito gonçalves'}, sendo necessário criá-lo pela função set()
#   {1, 2, 3, 4, 5, 6, 7, 8, 3, 3 , 3} conjunto de numeros
for nome in set('Joselito Gonçalves'):
    print(nome, end=',')
