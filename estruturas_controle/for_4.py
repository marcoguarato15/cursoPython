for i in range(1, 11):
    print(i)
else:
    print('fim')

#caso em que não será chamado o else do for, quando houver um break, saindo da iteração da estrutura de controle
    
for i in range(1, 11):
    if i == 6:
        break
    print(i)
else:
    print('fim')
    