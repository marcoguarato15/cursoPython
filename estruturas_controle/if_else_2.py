# coding= utf-8
def faixa_etaria(idade):
    if 0 <= idade < 18:
        return 'Menor de idade'
    elif idade in range(18, 60):
        return 'Adulto'
    elif idade in range(60, 100):
        return 'Melhor idade'
    elif idade >= 100:
        return 'Centenário'
    else:
        return 'Idade inválida'
    
if __name__ == '__main__':
    idades = (3, 18, 17, 46, 67, 105, -6)
    for idade in idades:
        print(f'Idade: {idade} -> {faixa_etaria(idade)}')