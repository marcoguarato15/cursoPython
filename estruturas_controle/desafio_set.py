PALAVRAS_PROIBIDAS = {'religião', 'politica', 'futebol'}
textos = ['João gosta de futebol e politica',
          'Maria gosta de basquete']

for texto in textos:
    intersection = PALAVRAS_PROIBIDAS.intersection(set(texto.lower().split()))
    if intersection:
        print('Texto recusado: ', intersection)
    else:
        print('Texto autorizado: ', texto)