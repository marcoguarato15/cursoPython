def switch_no_python(dia):
    match dia:
        case 2 | 3 | 4 | 5 | 6:
            return "Meio de semana"
        case 1 | 7:
            return "Fim de semana"
        case _:
            return "Dia inválido"


dia = input('informe o dia da semana: ')
print(switch_no_python(int(dia)))
