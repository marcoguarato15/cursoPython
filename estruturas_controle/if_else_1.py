#! python
# Conceitos      Notas
# A              De 10,0 à 9,1
# A-             De 9,0 à 8,1
# B              De 8,0 à 7,1
# B-             De 7,0 à 6,1
# C              De 6,0 à 5,1
# C-             De 5,0 à 4,1
# D              De 4,0 à 3,1
# D-             De 3,0 à 2,1
# E              De 2,0 à 1,1
# E-             De 1,0 à 0,0

import errno
import sys


class TerminalColor:
    ERROR = '\033[91m'
    NORMAL = '\033[0m'


def help():
    print('É necessário informar um número'
          + f'\n Sintaxe: {sys.argv[0]} <nota>')
    sys.exit(errno.EINVAL)


def valida_nota():
    if not sys.argv[1].isnumeric():
        help()
        print(TerminalColor.ERROR
              + 'Nota deve ser tipo numérico!'
              + TerminalColor.NORMAL)
        sys.exit(errno.EINVAL)
    else:
        nota = float(sys.argv[1])

    if (nota < 0 or nota > 10):
        print('Valor informado inválido, é necessário inserir um valor '
              + 'entre 0 e 10')
        sys.exit(errno.EINVAL)

    return nota


def relata_conceito(nota):
    if nota <= 10 and nota > 9:
        print('Conceito: A')
    elif nota <= 9 and nota > 8:
        print('Conceito: A-')
    elif nota <= 8 and nota > 7:
        print('Conceito: B')
    elif nota <= 7 and nota > 6:
        print('Conceito: B-')
    elif nota <= 6 and nota > 5:
        print('Conceito: C')
    elif nota <= 5 and nota > 4:
        print('Conceito: C-')
    elif nota <= 4 and nota > 3:
        print('Conceito: D')
    elif nota <= 3 and nota > 2:
        print('Conceito: D-')
    elif nota <= 2 and nota > 1:
        print('Conceito: E')
    else:
        print('Conceito: E-')


if __name__ == '__main__':
    if (len(sys.argv) < 2):
        help()

    relata_conceito(valida_nota())
