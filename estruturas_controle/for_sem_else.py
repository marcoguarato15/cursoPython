PALAVRAS_PROIBIDAS = ('futebol', 'política', 'religião')
textos = ['João gosta de futebol e política.',
          'Maria gosta de basquete.']

for texto in textos:
    for palavra in texto.lower().split():
        found = False
        if palavra in PALAVRAS_PROIBIDAS:
            found = True
            print('Este texto contem palavras proibídas: ', palavra)
            break
    
    if not found:
        print('Este texto não possui palavras proibídas: ', texto)
        