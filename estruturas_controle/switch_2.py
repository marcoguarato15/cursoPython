def dia_util(dia):
    dias  = {
        1: 'Fim de semana',
        2: 'Meio de semana',
        3: 'Meio de semana',
        4: 'Meio de semana',
        5: 'Meio de semana',
        6: 'Meio de semana',
        7: 'Fim de semana'
    }
    return dias.get(dia, 'número informado inválido')

if __name__ == '__main__':
    for dia in range(9):
        print(f'{dia}: {dia_util(dia)}')