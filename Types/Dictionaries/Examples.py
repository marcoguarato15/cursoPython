#wotks as a key : value typ, in wich the keys can be any Type (dict, str, bool, int...) or structure (objects, lists, dictionaries)
#is an indexed structure, having the worrysome of order.

dictionary = {'Name' : 'Joel', 'Age' : 16, 'courses' : ['english', 'postuguese']}
print(dictionary)

#methods included 'clear', 'copy', 'fromkeys', 'get', 'items', 'keys', 'pop', 'popitem', 'setdefault', 'update', 'values' and more.
#examples of methods.
#accessing via key like \'dictionary[15]\' is not good because it can be empty returning an error
#example of error
#print(dictionary[3])

#example of get(searches for value inside of the dictionary, not by index) with no response
print(dictionary.get(0, 'no key with this value'))
