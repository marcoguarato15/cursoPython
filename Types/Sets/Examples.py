#Set is an immutable list, without indexes (not subscriptable), wich means there is no worries with the concept of order.

a = {1, 2, 3, 4, 4 ,4} # === a = {1, 2, 3, 4}
b = {1, 2, 3}
c = {4, 5, 6, 7}


print('This is a set and if we try to acces by index there will occur an error! set \'a\' = {1, 2, 3, 4, 4, 4}:  ', a)
#impossible
#print(a[0])

#Its possible to use it with operator
print('like in sub set b <= a (\'b\' is a sub set of \'a\') returning a boolean: ', b <= a)

