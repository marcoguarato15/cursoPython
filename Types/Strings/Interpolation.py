#4 ways to interpolate
#! python
name, age, verified = 'Ana', 30, True

# %s -> string, %d -> integer, %f ->float, %r -> boolean
#Float and Int are usable, when it's a float it apears leveling to a higher number when >= .5 and to a lower when <= 4
#And when its a float it only gets the integer part of the number not leveling it to a higher or lower number acording to the decimal values.

#%.2f sets to use 2 decimal cases and it will level to a higher or lower number depending on wich decimal case is after the last shown.

print('Name: %s, Age: %.2f, Verified: %r' % (name, age, verified))
print('Name: {0}, Age: {1}, Verified: {2}'.format(name, age, verified)) #used before python 3.6
print(f'Name: {name}, Age: {age}, Verified: {verified}') #used after Python 3.6

#from string import Template
#there is a template format, necessary including as above
#temp = Template('Name: $n, Age: $a, Verified: $v')
#print(temp.substitute(n = name, a = age, v = verified))
