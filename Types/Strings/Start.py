#Strings are unchangable, you don't change it, you change the memory in wich it's located, altering the entire string.

print('''aspas simples triplas e \'simples no meio do texto 
    ... com quebra''')
document = "abas duplas com quebra de linha\n\t... e tab"

teste_primeira_frase = '''aspas simples triplas e \'simples no meio do texto 
    ... com quebra'''
print(teste_primeira_frase)

