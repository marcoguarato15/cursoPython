print('Here we can se that it is case sensitive and we can use member operators')
frase = 'Python is kinda easy to understand!'
print('frase: ', frase + 'py in frase:  '+ ' py' in frase)

#Shows the lenght(number of chars) in the string
print(len(frase))

print('frase.upper() Returns every letter to UPPER CASE and frase.lower() returns it in lower case (only changes the string if SET)')
print(frase.upper())
print(frase.lower())

print('Splits the frase removing the caracter specified with \' \' as default -> frase.split()')
print(frase.split())

#dir(str)          #returns the base __dir__ method if none returns the names in the scope, and if objects return attributes and methods.