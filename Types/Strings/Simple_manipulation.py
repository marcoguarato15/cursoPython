
#all the examples are from index value, starting at 0
name = 'Marco Antonio Guarato'

#chooses the caracter in the position 0 of the string printing the letter M
#print(name[0])
#prints the letter 'n' from Antonio
#print(name[7]) 
#negative number start from the end beggining with -1
#print(name[-1])

#you can also select numbers between ranges 

#print(name[6:]) #start at char in the position 6 ' ' without including it and writes till the end
#print(name[:6]) #prints the first 6 characters of the string
print(name[4:8]) #starts from the 4 index 0, 1, 2, 3, >4< then print to the 8 without including it
#print(name[2:15]) #start at position 2 without including it and reads ir till the end

#starting from the end you can reverse a string just like
#print(name[::-1]) #starts at the end counting 1 by 1
#print(name[0:6:2])# starts at the beggining stops at char 6 leaping one number 


#choose the letter x and z in the string 'lkjsghgyzçlj'
#string = 'lkjsghgyzçlj'
#string = string[7:9]
#print(string)
 