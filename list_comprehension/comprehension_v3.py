#gasta menos memória do que uma lista gerada pois gera o valor a medida que é preciso utilizar
generator = (i ** 2 for i in range(10) if i % 2 == 0)
print(next(generator))
print(next(generator))
print(next(generator))
print(next(generator))
print(next(generator))
#print(generator) não é um objeto subscritível, não podendo acessar sua posição
# print(next(generator)) irá gerar um erro por ter acabado na linha passada do metodo next