# -*- coding: utf-8 -*-

def get_tipo_dia(dia):
    dias  = {
        (1, 7): 'final de semana',
        tuple(range(2,7)): 'dia de semana',
    }
    dia_escolhido = (tipo for numero, tipo in dias.items() if dia in numero)
    return next(dia_escolhido, '** dia inválido')


if __name__ == '__main__':
    for dia in range(10):
        print(f'dia: {dia} é um ' + get_tipo_dia(dia))