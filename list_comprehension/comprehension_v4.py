#gasta menos memória do que uma lista gerada pois gera o valor a medida que é preciso utilizar
generator = (i ** 2 for i in range(10) if i % 2 == 0)

#o for já entende que o generator está sendo lido, sendo assim não é necessário
# chamar o método next

for number in generator:
    print(number)