# criando dictionary com list comprehension
# trocando para colchetes vira a criação de um dicionário


dicionario = {i: i * 2 for i in range(10) if i % 2 == 0}
print(dicionario)

'''
dicionario = {f'Item {i}': i * 2 for i in range(10) if i % 2 == 0}
print(dicionario)
'''

# keys  #values
for numero, dobro in dicionario.items():
    print(f'{numero} * 2 = {dobro}')
