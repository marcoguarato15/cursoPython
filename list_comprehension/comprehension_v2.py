# [ expressão for item i in list if condicional]


#com list comprehension
dobros = [ i ** 2 for i in range(1,11) if i % 2 == 0]
print(dobros)


#sem list comprehension
listaDobros = []
for i in range(1,11):
    if i % 2 == 0:
        listaDobros.append(i ** 2)

print(listaDobros)