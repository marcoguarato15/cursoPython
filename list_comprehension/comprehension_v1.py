# [ expressão for item i in list ]


#com list comprehension
dobros = [ i ** 2 for i in range(1,11)]
print(dobros)


#sem list comprehension
listaDobros = []
for i in range(1,11):
    listaDobros.append(i ** 2)

print(listaDobros)    