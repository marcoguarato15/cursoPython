#!/usr/bin/python3

class Humano:
    especie = 'Homo Sapiens'

    def __init__(self, nome):
        self.nome = nome
        self._idade = None

    def das_cavernas(self):
        self.especie = 'Homo Neanderthalensis'
        return self

    @staticmethod
    def especies():
        adjetivos = ('Habilis', 'Erectus', 'Neanderthalensis', 'Sapiens')
        return ('Australopteco',) + tuple(f'Homo {adj}' for adj in adjetivos)

    #ideia de polimorfismo, se duas classes filhas tentam utilizar esse metodo
    # ele funciona normalmente.
    @classmethod
    def is_evoluido(cls):
        return cls.especie == cls.especies()[-1]


class Neanderthal(Humano):
    especie = Humano.especies()[-2]


class HomoSapiens(Humano):
    especie = Humano.especies()[-1]

if __name__ == '__main__':

    jose = HomoSapiens('Jose')
    grokn = Neanderthal('Grokn')

    print (f'Evolução (a partir da classe): {", ".join(HomoSapiens.especies())}')
    print (f'Evolução (a partir da classe): {", ".join(jose.especies())}')

    print(f'Homo Sapiens evoluído? {HomoSapiens.is_evoluido()}')
    print(f'Neanderthal evoluído? {Neanderthal.is_evoluido()}')
    print(f'Jose Sapiens evoluído? {jose.is_evoluido()}')
    print(f'Grokn evoluído? {grokn.is_evoluido()}')
