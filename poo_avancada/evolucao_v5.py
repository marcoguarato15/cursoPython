#!/usr/bin/python3

class Humano:
    especie = 'Homo Sapiens'

    def __init__(self, nome):
        self.nome = nome
        self._idade = None

    def das_cavernas(self):
        self.especie = 'Homo Neanderthalensis'
        return self
    
    @property
    def inteligente(self):
        raise NotImplementedError('Propriedade não implementada')

    @property
    def idade(self):
        return self._idade
    
    @idade.setter
    def idade(self, idade):
        if idade < 0:
            raise ValueError('Idade deve ser um numero positivo')
        self._idade = idade
        
    @staticmethod
    def especies():
        adjetivos = ('Habilis', 'Erectus', 'Neanderthalensis', 'Sapiens')
        return ('Australopteco',) + tuple(f'Homo {adj}' for adj in adjetivos)

    #ideia de polimorfismo, se duas classes filhas tentam utilizar esse metodo
    # ele funciona normalmente.
    @classmethod
    def is_evoluido(cls):
        return cls.especie == cls.especies()[-1]


class Neanderthal(Humano):
    especie = Humano.especies()[-2]

    @property
    def inteligente(self):
        return False

class HomoSapiens(Humano):
    especie = Humano.especies()[-1]
    
    @property
    def inteligente(self):
        return True

if __name__ == '__main__':

    jose = HomoSapiens('Jose')
    jose.idade = 40
 
    grokn = Neanderthal('Grokn')
    print(f'Nome: {jose.nome}, idade: {jose.idade}')
    
    anonimo = Humano('John Doe')

    try:
        print(anonimo.inteligente())
    except NotImplementedError:
        print('propriedade abstrata')
        
    print('{} da classe {} é inteligente: {}'.format(jose.nome,\
                                                     jose.__class__.__name__,\
                                                     jose.inteligente))
    
    print('{} da classe {} é inteligente: {}'.format(grokn.nome,\
                                                     grokn.__class__.__name__,\
                                                     grokn.inteligente))