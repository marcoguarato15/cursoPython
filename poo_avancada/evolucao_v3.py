#!/usr/bin/python3

class Humano:
    especie = 'Homo Sapiens'

    def __init__(self, nome):
        self.nome = nome
        self._idade = None

    def das_cavernas(self):
        self.especie = 'Homo Neanderthalensis'
        return self
    
    def get_idade(self):
        return self._idade
    
    def set_idade(self, idade):
        if idade < 0:
            raise ValueError('Idade deve ser um numero positivo')
        self._idade = idade
        
    @staticmethod
    def especies():
        adjetivos = ('Habilis', 'Erectus', 'Neanderthalensis', 'Sapiens')
        return ('Australopteco',) + tuple(f'Homo {adj}' for adj in adjetivos)

    #ideia de polimorfismo, se duas classes filhas tentam utilizar esse metodo
    # ele funciona normalmente.
    @classmethod
    def is_evoluido(cls):
        return cls.especie == cls.especies()[-1]


class Neanderthal(Humano):
    especie = Humano.especies()[-2]


class HomoSapiens(Humano):
    especie = Humano.especies()[-1]

if __name__ == '__main__':

    jose = HomoSapiens('Jose')
    jose.set_idade(40)
 
    print(f'Nome: {jose.nome}, idade: {jose.get_idade()}')