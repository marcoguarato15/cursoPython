#!/usr/bin/python3
class RGB:
    def __init__(self) -> bool:
        self.cores = ['red', 'blue','green'][::-1] 

    # Sem esta função não é possivel iterar em um 'for'
    def __iter__(self):
        return self
    
    def __next__(self):
        try:
            return self.cores.pop()
        except IndexError:
            raise StopIteration('Acabaram as cores')
        

if __name__ == '__main__':
    cores = RGB()
    print(next(cores))
    print(next(cores))
    print(next(cores))
    
    try:
        print(next(cores))
    except StopIteration:
        print('Acabou!')