def fibonacci(sequencia=[0, 1]):
    sequencia.append(sequencia[-1] + sequencia[-2])
    return sequencia

if __name__ == '__main__':
    inicio = fibonacci()
    print(inicio, id(inicio))
    print( fibonacci(inicio))

    restart = fibonacci()
    print(restart, id(restart))
    assert restart == [0, 1, 1]

#há o problema com parâmetros mutáveis que ele não instancia um objeto novamente, ele 
#    simplesmente pega a nova informação do objeto com a mesma memória do anterior, gerando essa incongruencia