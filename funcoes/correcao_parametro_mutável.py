#há o problema com parâmetros mutáveis que ele não instancia um objeto novamente, ele 
#    simplesmente pega a nova informação do objeto com a mesma memória do anterior, gerando essa incongruencia

#problema resolvido com esta verificação
def fibonacci(sequencia=None):
    sequencia = sequencia or [0, 1]
    sequencia.append(sequencia[-1] + sequencia[-2])
    return sequencia

if __name__ == '__main__':
    inicio = fibonacci()
    print(inicio, id(inicio))
    print( fibonacci(inicio))

    restart = fibonacci()
    print(restart, id(restart))
    assert restart == [0, 1, 1]

# nota-se que neste exemplo com o parametro padrão não sendo um atributo mutável (lista, vetor, objeto, dicionário) e sim um valor vazio 
# para depois verificar se está com algum valor ou definir esse valor de atributo mutável. 
# não acontece de salvar o local na memória, assim sendo um dos modos de resolver
# Outro modo seria em vez de um dicionário, utilizar de uma tupla como é feito no exemplo em fundamentos_projetos fibonacci.