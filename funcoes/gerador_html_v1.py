def gerador_html(texto, classe='success'):
    return f'<div class="{classe}">{texto}</div>'


if __name__ == '__main__':
    # (teste) Assert retorna nada se verdadeiro e se falso retorna um erro 'AssertionError'
    assert gerador_html('texto inserido') == \
        '<div class="success">texto inserido</div>'

    assert (gerador_html('texto', 'error')) == \
        '<div class="error">texto</div>'
