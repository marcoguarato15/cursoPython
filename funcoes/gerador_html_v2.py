def gerador_html(texto, classe='success', inline=False):
    tag = 'div'
    if inline: tag = 'span'
    return f'<{tag} class="{classe}">{texto}</{tag}>'


if __name__ == '__main__':
    print(gerador_html('texto sem inline'))
    print(gerador_html('texto com inline', inline=True))
    