def tag_bloco(texto, classe='success', inline=False):
    tag = '<span>' if inline else '<div>'
    return f'<{tag} class="{classe}">{texto}</{tag}>'

def tag_lista(*itens):
    lista = ''.join(f'<li>{item}</li>' for item in itens)
    return f'<ul>{lista}</ul>'


if __name__ == '__main__':
    print(tag_bloco('texto sem inline'))
    print(tag_bloco('texto com inline', inline=True))
    print(tag_bloco(tag_lista("Item1", "Item2"), classe="error"))

    print(tag_lista('ana', 'julia', 'marco', 'manke'))