bloco_atrs = ('bloco_accesskey', 'bloco_id')
ul_atrs = ('ul_id', 'ul_style')


# generator 
# realiza algo (f'{k.split("_")[-1]}="{v}"') para um for ( k, v in informados.items() )
# com uma condição se necessário if k in suportados
def get_atrs(informados, suportados):
    return ''.join(f'{k.split("_")[-1]}="{v}"'
                   for k, v in informados.items() if k in suportados)


def tag_bloco(conteudo, *args , classe='success', inline=False, **novos_atrs):

    tag = 'span' if inline else 'div'
    html = conteudo if not callable(conteudo) else conteudo(*args, **novos_atrs)
    return f'<{tag} {get_atrs(novos_atrs, bloco_atrs)} class="{classe}">{html}</{tag}>'

def tag_lista(*itens, **novos_atrs):
    lista = ''.join(f'<li>{item}</li>' for item in itens)
    return f'<ul {get_atrs(novos_atrs, ul_atrs)}>{lista}</ul>'


if __name__ == '__main__':
    print(tag_bloco('texto sem inline'))
    print(tag_bloco('texto com inline', inline=True))
    print(tag_bloco(tag_lista("Item1", "Item2"), classe="error"))

    print(tag_bloco(tag_lista('isso é um conteudo da lista', 'isso é outro conteudo da lista'
                              , ul_id='lista', ul_style='color:red'), classe='info',
                              bloco_acesskey='m', bloco_id='conteudo'))