# empacotou os valores do dicionario teste em teste
def unpack(*teste):
    for nome, sobrenome in teste:
        print(f'Nome: {nome}, Sobrenome: {sobrenome}')


# desempacotou os valores da tupla nas variaveis a, b, c
def somaSoTres(a, b, c):
    return a + b + c


def somaNumeros(*numeros):
    soma = 0
    for n in numeros:
        soma += n
    return soma


if __name__ == '__main__':
    # exemplo de empacotamento, packing
    teste = {'nome': "marco", 'sobrenome': "antonio"}
    print(somaNumeros(1, 2, 3, 4, 5, 6))

    # exemplo do desempacotamento, na função terá os valores a, b ,c 
    # definidos nelas mesmas
    tupla = (1, 2, 3)
    print(somaSoTres(*tupla))
