from typing import Any


class Potencia:
    #Calcula a potencia específica
    #self referece a instancia atual do objeto
    def __init__(self, expoente):
        self.expoente = expoente

    def __call__(self, base):
        return base ** self.expoente
    
if __name__ == "__main__":
    elQuadrado = Potencia(2)
    elCubo = Potencia(3)

    if callable(elQuadrado) and callable(elCubo):
        print(f'5 elevado ao quadrado => {elQuadrado(5)}')
        print(f'3 elevado ao cubo => {elCubo(3)}')