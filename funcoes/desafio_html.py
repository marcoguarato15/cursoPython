def tag(tag, *args, **kwargs):
    if 'html_class' in kwargs:
        #realiza o pop que retorna o valor do html_class e o coloca como valor para a chave 'class'
        kwargs['class'] = kwargs.pop('html_class') 
        
    attrs = ''.join(f'{k}="{v}" ' for k, v in kwargs.items())
    inner = ''.join(args)

    return f'<{tag} {attrs}>{inner}</{tag}>'

if __name__ == "__main__":
    print(
        tag('p',
            tag('span', 'Curso de python 3, por '),
            tag('strong', 'Juracy filho', id='jf'),
            tag('span', ' e '),
            tag('strong', 'Leonardo Leitão', id='ll'),
            html_class='alert'
            )
    )