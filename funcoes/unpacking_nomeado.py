# **kwargs argumentos nomeados (keyworded args)

def resultado_f1(**podium):
    for posição, piloto in podium.items():
        print(f'{posição} => {piloto}')


if __name__ == '__main__':
    resultado_f1(
        primero="Hamiltinho",
        segundo="Verstapo",
        terceiro="Lucola"
    )

    