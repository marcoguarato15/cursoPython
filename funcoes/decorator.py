# Padrao decorator em python
def log(function):
    def decorator(*args, **kwargs):
        print(f'Inicio da fução: {function.__name__}')
        print(f'args: {args}')
        print(f'ksargs: {kwargs}')
        resultado = function(*args, **kwargs)
        print(f'resultado da chamada : {resultado}')
        return resultado
    return decorator


@log
def soma(x, y):
    return x + y


@log
def subtracao(x, y):
    return x - y


if __name__ == "__main__":
    print(soma(1, 2))
    print(subtracao(4, y=-4))
