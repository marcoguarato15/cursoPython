from datetime import datetime

class Projeto:
    def __init__(self, nome):
        self.nome = nome
        self.tarefas = []

    def add(self, descricao):
        self.tarefas.append(Tarefa(descricao))

    def pendentes(self):
        return [tarefas for tarefas in self.tarefas if tarefas.feito == False]
    
    def consulta(self, descricao):
        #o [0] no final retorna a tarefa encontrada, pois o resultado dessa list_comprehension é uma lista
        return [tarefa for tarefa in self.tarefas if tarefa.descricao == descricao][0]

    def __str__(self):
        return f'{self.nome} "{len(self.pendentes())}" tarefas pendentes'

 
class Tarefa:
    def __init__(self, descricao):
        self.descricao = descricao
        self.feito = False
        self.criacao = datetime.now()

    def concluir(self):
        self.feito = True

    def __str__(self):
        return self.descricao + ( ' (Concluída)' if self.feito else '' )
    
def main():
    casa = Projeto('Tarefas de casa')
    casa.add('Lavar roupas')
    casa.add('Lavar pratos')
    
    casa.consulta('Lavar roupas').concluir()

    for tarefas in casa.tarefas:
        print(f'- {tarefas}')

    print(casa)

if __name__ == '__main__':
    main()