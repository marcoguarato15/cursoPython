from datetime import datetime, timedelta


class Projeto:
    def __init__(self, nome):
        self.nome = nome
        self.tarefas = []

    # Adição do volume 3, iteração, removendo casa.tarefas, para apenas casa
    def __iter__(self):
        return self.tarefas.__iter__()

    def add(self, descricao, data_vencimento=None):
        self.tarefas.append(Tarefa(descricao, data_vencimento))

    def pendentes(self):
        return [tarefas for tarefas in self.tarefas if tarefas.feito == False]

    def consulta(self, descricao):
        # o [0] no final retorna a tarefa encontrada, pois o resultado dessa list_comprehension é uma lista
        return [tarefa for tarefa in self.tarefas if tarefa.descricao == descricao][0]

    def __str__(self):
        return f'{self.nome} "{len(self.pendentes())}" tarefas pendentes'


class Tarefa:
    def __init__(self, descricao, vencimento=None):
        self.descricao = descricao
        self.feito = False
        self.criacao = datetime.now()
        self.vencimento = vencimento

    def concluir(self):
        self.feito = True

    def __str__(self):
        status = []
        if self.feito:
            status.append(' (Concluída)')
        elif self.vencimento:
            if datetime.now() > self.vencimento:
                status.append(' (Vencida)')
            else:
                dias = (self.vencimento - datetime.now()).days
                status.append(f' !-dias para vecer: {dias}-!')
        
        return f'{self.descricao} ' + ''.join(status) 


def main():
    casa = Projeto('Tarefas de casa')
    casa.add('Lavar roupas', datetime.now() + timedelta(days=1, seconds=1))
    casa.add('Lavar pratos')

    # casa.consulta('Lavar roupas').concluir()
    mercado = Projeto('Compras do Mercado')
    mercado.add('Carne')
    mercado.add('Arroz')
    mercado.add('Feijao')
    mercado.consulta('Feijao').concluir()
    mercado.add('Batata', datetime.now() + timedelta(days=4, seconds=1))
    # mudança v3, iterando sem adicionar casa.tarefas, e sim apenas casa
    for tarefas in casa:
        print(f'- {tarefas}')

    print()
    for compras in mercado:
        print(f'- {compras}')

    print(casa)
    print(mercado)

if __name__ == '__main__':
    main()
