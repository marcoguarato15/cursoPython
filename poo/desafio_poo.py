# REGRAS do Aplicativo

# tanto vendedor quanto cliente são pessoas

# Cliente possui lista de compras do tipo compra
# metodo Cliente.registra.... receve um objeto Compra
# Propriedade Compra.vendedor e do tipo Vendedor

# ao converter cliente ou vendedor em string deve mostrar o nome e a idade
# metodo Cliente.total... retoran somatorio de todas compras
# Cliente get_data..... retorna a data da ultima compra
from loja import Vendedor
from loja import Cliente
from loja import Compra


if __name__ == '__main__':
    pass

    vend1 = Vendedor('Marco', 18, 1800)
    vend2 = Vendedor('Antonio', 20, 1400)

    cli1 = Cliente('Joao', 18)
    cli2 = Cliente('Maria',17)

    cli1.registrar_compra(Compra(500, vendedor=vend1))
    cli1.registrar_compra(Compra(100, vendedor=vend2))
    cli2.registrar_compra(Compra(200, vendedor=vend2))


    print(f'data da ultima compra do cliente {cli1}: ' + str(cli1.get_data_ultima_compra()))
    print(f'data da ultima compra do cliente {cli2}: '+ str(cli2.get_data_ultima_compra()))

    print()

    print(f'Cliente {cli2} é adulto? >>' + str(cli2.is_adulto()))
    print(f'Cliente {cli1} é adulto? >>' + str(cli1.is_adulto()))

    print()

    print(f'Total de compras: {cli1.total_compras()}  do cliente: {cli1}')
    print(f'Total de compras: {cli2.total_compras()} do Cliente: {cli2}')

    print()

    print('Cliente 1:', cli1)
    print('Cliente 2:', cli2)
