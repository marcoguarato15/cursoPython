from .Pessoa import Pessoa
from .Compra import Compra

def get_data(compra):
    return compra.data

# cliente compras[]
# construtor, registrar_compra, get_data_ultima_compra, total_compras
class Cliente(Pessoa):
    def __init__(self, nome, idade):
        super().__init__(nome, idade)
        self.compras = []

    def registrar_compra(self, compra=Compra()):
        self.compras.append(compra)

    def get_data_ultima_compra(self):
        #posso passar a key=get_data como lambda c: c.data
        return sorted(self.compras, key=get_data)[-1].data
    
    def total_compras(self):
        total = 0
        for compra in self.compras:
            total += compra.valor
        return (total or 0)