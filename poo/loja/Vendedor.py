from .Pessoa import Pessoa

# vendedor salario
# construtor (salario)
class Vendedor(Pessoa):
    def __init__(self, nome='', idade=0, salario=0):
        super().__init__(nome, idade)
        self.salario = salario
