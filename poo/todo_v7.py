from datetime import datetime, timedelta


class Projeto:
    def __init__(self, nome):
        self.nome = nome
        self.tarefas = []

    # Adição do volume 3, iteração, removendo casa.tarefas, para apenas casa
    def __iter__(self):
        return self.tarefas.__iter__()

    # sobrecarga do operador +=
    # projeto += tarefa
    # casa += tarefa_criada
    def __iadd__(self, tarefa):
        tarefa.dono += self
        self._add_tarefa(tarefa)
        return self


    def _add_tarefa_recorrente(self, tarefa, **kwargs):
        self.tarefas.append(tarefa)

    def _add_tarefa(self, descricao, **kwargs):
        self.tarefas.append(Tarefa(descricao, kwargs.get('vencimento', None)))

    def add(self, tarefa, vencimento=None, **kwargs):
        funcao_escolhida = self._add_tarefa_recorrente if isinstance(tarefa, Tarefa) \
            else self._add_tarefa
        #setando o valor vencimento para a chave vencimento ('vencimento': vencimento)
        kwargs['vencimento'] = vencimento
        funcao_escolhida(tarefa, **kwargs)

    def pendentes(self):
        return [tarefas for tarefas in self.tarefas if tarefas.feito == False]

    def consulta(self, descricao):
        # o [0] no final retorna a tarefa encontrada, pois o resultado dessa list_comprehension é uma lista
        return [tarefa for tarefa in self.tarefas if tarefa.descricao == descricao][0]

    def __str__(self):
        return f'{self.nome} "{len(self.pendentes())}" tarefas pendentes'


class Tarefa:
    def __init__(self, descricao, vencimento=None):
        self.descricao = descricao
        self.feito = False
        self.criacao = datetime.now()
        self.vencimento = vencimento

    def concluir(self):
        self.feito = True

    def __str__(self):
        status = []
        if self.feito:
            status.append(' (Concluída)')
        elif self.vencimento:
            if datetime.now() > self.vencimento:
                status.append(' (Vencida)')
            else:
                dias = (self.vencimento - datetime.now()).days
                status.append(f' !-dias para vecer: {dias}-!')
        
        return f'{self.descricao} ' + ''.join(status) 

class TarefaRecorrente(Tarefa):
    def __init__(self, descricao, vencimento=None, dias=7):
        super().__init__(descricao, vencimento)
        self.dias = dias
        self.dono = None
        
    def concluir(self):
        super().concluir()
        novo_vencimento = datetime.now() + timedelta(days=self.dias)
        nova_tarefa = TarefaRecorrente(self.descricao, novo_vencimento, self.dias)
        if self.dono:
            self.dono += nova_tarefa
        return nova_tarefa

def main():
    casa = Projeto('Tarefas de casa')
    casa.add('Lavar roupas', datetime.now() + timedelta(days=1, seconds=1))
    casa.add('Lavar pratos')

    casa.add(TarefaRecorrente('Trocar lençóis', datetime.now()).concluir())
    

    # casa.consulta('Lavar roupas').concluir()
    mercado = Projeto('Compras do Mercado')
    mercado.add('Carne')
    mercado.add('Arroz')
    mercado.add('Feijao')
    mercado.consulta('Feijao').concluir()
    mercado.add('Batata', datetime.now() + timedelta(days=4, seconds=1))
    mercado.add(TarefaRecorrente('Arroz', vencimento=datetime.now() ,dias=30).concluir())
    # mudança v3, iterando sem adicionar casa.tarefas, e sim apenas casa
    for tarefas in casa:
        print(f'- {tarefas}')

    print()
    for compras in mercado:
        print(f'- {compras}')

    print(casa)
    print(mercado)

if __name__ == '__main__':
    main()
