def fibonacci(quantidade):
    resultado = [0, 1]
    for _ in range(quantidade -2):
        resultado.append(sum(resultado[-2:]))
    return resultado

if __name__ == '__main__':
    #Listar os 50 primeiros numeros da sequencia
    resultado = fibonacci(50)
    for numero in resultado:
        print(numero)
        