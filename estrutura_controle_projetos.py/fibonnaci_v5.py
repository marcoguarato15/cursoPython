def fibonacci(limite):
    resultado = [0, 1]
    while resultado[-1] < limite:
        resultado.append(sum(resultado[-2:]))

    return resultado


if __name__ == '__main__':
    resultado = fibonacci(100000)
    for numero in resultado:
        print(numero)
        