def fibonacci(quantidade):
    resultado = [0, 1]
    while True:
        resultado.append(sum(resultado[-2:]))
        if len(resultado) == quantidade:
            break
    return resultado


if __name__ == '__main__':
    #Listar os 50 primeiros numeros da sequencia
    resultado = fibonacci(50)
    for numero in resultado:
        print(numero)
        