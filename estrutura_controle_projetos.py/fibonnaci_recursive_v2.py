def fibonacci(quantidade, sequencia=(0, 1)):
    
    return sequencia if len(sequencia) == quantidade else \
        fibonacci(quantidade, sequencia + (sum(sequencia[-2:]),))


if __name__ == '__main__':
    #limite de recursividade atingido em 1000
    for fib in fibonacci(15):
        print(fib)
